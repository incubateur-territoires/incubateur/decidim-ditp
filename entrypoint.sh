#!/bin/bash
set -e

# Remove a potentially pre-existing server.pid for Rails.
if [ -f /app/tmp/pids/server.pid ]; then
  rm /app/tmp/pids/server.pid
fi

# Quick hack: launch sidekiq and rails in same container
sidekiq -C config/sidekiq.yml &
rails server -b 0.0.0.0 &

wait -n

exit $?
